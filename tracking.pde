import processing.video.*;
import java.util.List;

/****************** GLOBAL VARS ******************/

// Instance of the video capturing object.
Capture video;

// The color we wish to track.
color trackColor1; 
color trackColor2;
color trackColor3;
boolean color1Defined = false;
boolean color2Defined = false;
boolean color3Defined = false;

// Color match threshold.
float colorThreshold = 50.0;

// Capture dimensions
int captureWidth = 640;
int captureHeight = 480;

// Last drawn pixel - needed for making connected lines
Pixel lastDrawnPixel = new Pixel(color(0,0,0), -100, -100);
float pixelConnectionThreshold = 15.0;

// Accuracy of tracking - WARNING: very computational intensive - use 0.5 or less
float trackingAccuracy = 0.4;


/****************** SETUP ******************/
void setup() {
  size(captureWidth * 2, captureHeight);
  
  // Fill only the left half of the screen with the video.
  // The right half is for drawing.
  video = new Capture(this, width/2, height);
  video.start();
  
  // Initialize tracking color. Track transparent at start.
  trackColor1 = color(0, 0, 0, 255);
  
  // Set canvas to white.
  background(255);
}




/****************** DRAW *********************/
void draw() {
  
  refreshVideo();

  if (color1Defined && color2Defined && color3Defined) {
    List<Pixel> color1Pixels = findPixelsOfColor(trackColor1, colorThreshold);
    List<Pixel> color2Pixels = findPixelsOfColor(trackColor2, colorThreshold);
    List<Pixel> color3Pixels = findPixelsOfColor(trackColor3, colorThreshold);
    
    if (color1Pixels.size() == 0) {
      System.out.println("Unable to detect color 1 marker.");
    } else if (color2Pixels.size() == 0) {
      System.out.println("Unable to detect color 2 marker.");
    } else if (color3Pixels.size() == 0) {
      System.out.println("Unable to detect color 3 marker.");
    } else {
      PixelTris closestPixels = findClosesPixels(color1Pixels, color2Pixels, color3Pixels);
      Pixel centerPixel = getCenterPixel(closestPixels);
      
      displayDetectionMarkerOnVideo(trackColor1, closestPixels.pixel1.x, closestPixels.pixel1.y);
      displayDetectionMarkerOnVideo(trackColor2, closestPixels.pixel2.x, closestPixels.pixel2.y);
      displayDetectionMarkerOnVideo(trackColor3, closestPixels.pixel3.x, closestPixels.pixel3.y);
      displayDetectionMarkerOnVideo(color(255,255,0), centerPixel.x, centerPixel.y);
      
      continuousDraw(color(0,0,0), centerPixel);
    }
        
  }

}



/****************** TRACKING ******************/

/* Class pixel */
class Pixel {
  public Pixel (color c, int x, int y) { clr = c; this.x = x; this.y = y; }
  public int x, y;
  public color clr;
}

/* Class pixel pair */
class PixelTris { public Pixel pixel1, pixel2, pixel3; public PixelTris(Pixel p1, Pixel p2, Pixel p3) { pixel1 = p1; pixel2 = p2; pixel3 = p3; } }

/* Find pixels from current video frame that are matching the tracking color with a threshold */
List<Pixel> findPixelsOfColor(color c, float colorDistanceThreshold) {
 List<Pixel> colorPixels = new ArrayList<Pixel>(); 
  
  for (int x = 0; x < video.width; x ++ ) {
    for (int y = 0; y < video.height; y ++ ) {
      // Get pixel color.
      int loc = x + y*video.width;
      color currentColor = video.pixels[loc];
      
      float r1, g1, b1, r2, g2, b2;
      
      // Parse both colors.
      r1 = red(currentColor);
      g1 = green(currentColor);
      b1 = blue(currentColor);
      
      r2 = red(c);
      g2 = green(c);
      b2 = blue(c);

      // Compare colors with euclidean.
      float colorDistance = dist(r1, g1, b1, r2, g2, b2);

       if (colorDistance <= colorDistanceThreshold) {
         colorPixels.add(new Pixel(currentColor, x, y));
       }
    }
  }
  
  return colorPixels;
}

/* Find a pair of the three closest pixels: one from each list (=each color) */
PixelTris findClosesPixels(List<Pixel> color1Pixels, List<Pixel> color2Pixels, List<Pixel> color3Pixels) {
  
  float minimalDistance = Float.POSITIVE_INFINITY;
  Pixel list1ClosestPixel = null, list2ClosestPixel = null, list3ClosestPixel = null;
  
  float accuracyLimit = 3.0 * trackingAccuracy;
  
  for (Pixel p1 : color1Pixels) {
   for (Pixel p2 : color2Pixels) {
     
     if (dist(p1.x, p1.y, p2.x, p2.y) > minimalDistance*accuracyLimit)
       continue;
     
     for (Pixel p3 : color3Pixels) {
      float distance = ( dist(p1.x, p1.y, p2.x, p2.y) + dist(p1.x, p1.y, p3.x, p3.y) + dist(p3.x, p3.y, p2.x, p2.y) ) / 3;
      
      if (distance < minimalDistance) {
        minimalDistance = distance;
        list1ClosestPixel = p1;
        list2ClosestPixel = p2;
        list3ClosestPixel = p3;
      }  
      
     }  
   }    
  }

  PixelTris closestPixels = new PixelTris(list1ClosestPixel, list2ClosestPixel, list3ClosestPixel);
  
  return closestPixels;
}

/* Calculate the virtual center of the tris of the three pixels */
Pixel getCenterPixel(PixelTris closestPixels) {
  
  // Steady points are the middle points between two points (lie in the middle of the line connecting the two points)
  
  float steadyPoint1X = (float)( closestPixels.pixel1.x + closestPixels.pixel2.x ) / 2;
  float steadyPoint1Y = (float)( closestPixels.pixel1.y + closestPixels.pixel2.y ) / 2;
  
  float steadyPoint2X = (float)( closestPixels.pixel1.x + closestPixels.pixel3.x ) / 2;
  float steadyPoint2Y = (float)( closestPixels.pixel1.y + closestPixels.pixel3.y ) / 2;
  
  // Imagine we have these steady points connected to opposite vertexes
  // The crossing point would be where these lines would cross
  // steady point 1's opposite vertex is point 3
  // steady point 2's opposite vertex is point 2
  
  int xcenter = (int)( (steadyPoint1X + steadyPoint2X)/2 + ((float)closestPixels.pixel3.x + closestPixels.pixel2.x)/2 ) / 2;
  int ycenter = (int)( (steadyPoint1Y + steadyPoint2Y)/2 + ((float)closestPixels.pixel3.y + closestPixels.pixel2.y)/2 ) / 2;
  
  return new Pixel(color(0,0,0, 255), xcenter, ycenter); //color not used
}

/* Draw to whiteboard */
void drawOnWhiteboard(color clr, int x, int y) {
   noStroke();
   fill(clr);
   ellipse(captureWidth * 2 - x, y, 5, 5); // mirror image
}

/* Add marker to video */
void displayDetectionMarkerOnVideo(color clr, int x, int y) {
  fill(clr);
  strokeWeight(2.0);
  stroke(0);
  ellipse(x, y, 8, 8);
}

/* Continuous draw - draws next dot and connects it with previous dot if it's close enough */
void continuousDraw(color clr, Pixel endPixel) {
  float distance = dist(lastDrawnPixel.x, lastDrawnPixel.y, endPixel.x, endPixel.y);
  if (distance > pixelConnectionThreshold) {
    drawOnWhiteboard(clr, endPixel.x, endPixel.y);
  } else {
   int xdiff = endPixel.x - lastDrawnPixel.x;
   int ydiff = endPixel.y - lastDrawnPixel.y;
   
   if (abs(xdiff) > abs(ydiff)) {
     // progress by each y pixel
     for (int i = 0; i < abs(ydiff); i++) {
       int yoffset = i * ydiff/abs(ydiff); // get sign right!
       int xoffset = (int) ( (float)abs(xdiff)/(float)abs(ydiff) * i ) * xdiff/abs(xdiff);
       drawOnWhiteboard(clr, lastDrawnPixel.x + xoffset, lastDrawnPixel.y + yoffset);
     }
   } else {
     for (int i = 0; i < abs(xdiff); i++) {
       int xoffset = i * xdiff/abs(xdiff); // get sign right!
       int yoffset = (int) ( (float)abs(ydiff)/(float)abs(xdiff) * i ) * ydiff/abs(ydiff);
       drawOnWhiteboard(clr, lastDrawnPixel.x + xoffset, lastDrawnPixel.y + yoffset);
     }
   }
   
  }
  
  lastDrawnPixel = endPixel;  
}



/****************** VIDEO ******************/
void captureEvent(Capture video) {
  // Read image from the camera
  video.read();
}

void refreshVideo() {
  video.loadPixels();
  image(video, 0, 0, width/2, height);
}




/******************  MOUSE EVENTS ******************/
void mousePressed() {
  
  // Reset colors if already defined
  if (color1Defined && color2Defined && color3Defined) {
    color1Defined = false;
    color2Defined = false;
    color3Defined = false;
  }
  
  if (!color1Defined) {
      // Save color where the mouse is clicked in trackColor variable
      int loc = mouseX + mouseY*video.width; 
      trackColor1 = video.pixels[loc];
      println(trackColor1);
      println("Color 1 defined.");
      color1Defined = true;
  } else {
    if (!color2Defined) {
        int loc = mouseX + mouseY*video.width; 
        trackColor2 = video.pixels[loc];
        println(trackColor2);
        println("Color 2 defined.");
        color2Defined = true;
    } else {
        int loc = mouseX + mouseY*video.width; 
        trackColor3 = video.pixels[loc];
        println(trackColor3);
        println("Color 3 defined.");
        color3Defined = true;
    }
  }

}




/****************** KEYBOARD EVENTS ******************/
void keyPressed() {
  if (key == ' ') {  // Catch the spacebar pressed event.
    print("Clear");
    background(255);
  }
}
